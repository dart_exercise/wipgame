import 'dart:io';
import 'Boss.dart';
import 'Finish.dart';
import 'Monster.dart';
import 'Star.dart';
import 'Treasure.dart';
import 'Wizard.dart';
import 'TableMap.dart';
import 'Store.dart';

String direction = "";
String inputDirection() {
  print("Input direction : ");
  direction = stdin.readLineSync()!;
  return direction;
}

class Game {
  late TableMap map;
  late Store store;

  Game() {}

  void playGame() {
    //Create TableMap & Set index ob List<Object>
    TableMap map = new TableMap();
    map.setListObj();
    //Create Wizard
    Wizard wizard = new Wizard("W", 0, 1, map);
    //Create Store
    Store store1 = new Store(5, 1);
    Store store2 = new Store(27, 3);
    Store store3 = new Store(5, 4);
    Store store4 = new Store(22, 6);
    //Create Treasure
    Treasure treasure1 = new Treasure(4, 1);
    Treasure treasure2 = new Treasure(25, 2);
    Treasure treasure3 = new Treasure(6, 3);
    Treasure treasure4 = new Treasure(19, 3);
    Treasure treasure5 = new Treasure(21, 3);
    Treasure treasure6 = new Treasure(1, 6);
    //Create Monster
    Monster monster1 = new Monster(10, 1, 100);
    Monster monster2 = new Monster(13, 2, 100);
    Monster monster3 = new Monster(26, 2, 100);
    Monster monster4 = new Monster(17, 3, 100);
    Monster monster5 = new Monster(20, 3, 100);
    Monster monster6 = new Monster(7, 4, 100);
    Monster monster7 = new Monster(26, 4, 100);
    Monster monster8 = new Monster(4, 5, 100);
    Monster monster9 = new Monster(11, 5, 100);
    Monster monster10 = new Monster(21, 5, 100);
    Monster monster11 = new Monster(24, 6, 100);

    //Create Boss
    Boss boss = new Boss(25, 6); //25, 6
    //Create Finish
    Finish finish = new Finish(28, 6);
    bool win = false;
    bool status = false, loseMon = false, loseBoss = false;

    //Set Wizard
    map.setWizard(wizard);
    //Set Store
    map.setStore(store1);
    map.setStore(store2);
    map.setStore(store3);
    map.setStore(store4);
    //Set Treasure
    map.setTreasure(treasure1);
    map.setTreasure(treasure2);
    map.setTreasure(treasure3);
    map.setTreasure(treasure4);
    map.setTreasure(treasure5);
    map.setTreasure(treasure6);
    //Set Monster
    map.setMonster(monster1);
    map.setMonster(monster2);
    map.setMonster(monster3);
    map.setMonster(monster4);
    map.setMonster(monster5);
    map.setMonster(monster6);
    map.setMonster(monster7);
    map.setMonster(monster8);
    map.setMonster(monster9);
    map.setMonster(monster10);
    map.setMonster(monster11);

    //Set Boss
    map.setBoss(boss);
    //Set Finish
    map.setFinish(finish);
    //Create Star
    Star star1 = new Star(0, 0);
    Star star2 = new Star(1, 0);
    Star star3 = new Star(2, 0);
    Star star4 = new Star(3, 0);
    Star star5 = new Star(4, 0);
    Star star6 = new Star(5, 0);
    Star star7 = new Star(6, 0);
    Star star8 = new Star(7, 0);
    Star star9 = new Star(8, 0);
    Star star10 = new Star(9, 0);
    Star star11 = new Star(10, 0);
    Star star12 = new Star(11, 0);
    Star star13 = new Star(12, 0);
    Star star14 = new Star(13, 0);
    Star star15 = new Star(14, 0);
    Star star16 = new Star(15, 0);
    Star star17 = new Star(16, 0);
    Star star18 = new Star(17, 0);
    Star star19 = new Star(18, 0);
    Star star20 = new Star(19, 0);
    Star star21 = new Star(20, 0);
    Star star22 = new Star(21, 0);
    Star star23 = new Star(22, 0);
    Star star24 = new Star(23, 0);
    Star star25 = new Star(24, 0);
    Star star26 = new Star(25, 0);
    Star star27 = new Star(26, 0);
    Star star28 = new Star(27, 0);
    Star star29 = new Star(28, 0);
    Star star30 = new Star(29, 0);
    Star star31 = new Star(25, 1);
    Star star32 = new Star(26, 1);
    Star star33 = new Star(27, 1);
    Star star34 = new Star(28, 1);
    Star star35 = new Star(29, 1);
    Star star36 = new Star(1, 2);
    Star star37 = new Star(2, 2);
    Star star38 = new Star(4, 2);
    Star star39 = new Star(5, 2);
    Star star40 = new Star(6, 2);
    Star star41 = new Star(7, 2);
    Star star42 = new Star(8, 2);
    Star star43 = new Star(9, 2);
    Star star44 = new Star(10, 2);
    Star star45 = new Star(14, 2);
    Star star46 = new Star(15, 2);
    Star star47 = new Star(16, 2);
    Star star48 = new Star(17, 2);
    Star star49 = new Star(18, 2);
    Star star50 = new Star(19, 2);
    Star star51 = new Star(20, 2);
    Star star52 = new Star(21, 2);
    Star star53 = new Star(22, 2);
    Star star54 = new Star(23, 2);
    Star star55 = new Star(24, 2);
    Star star56 = new Star(27, 2);
    Star star57 = new Star(28, 2);
    Star star58 = new Star(29, 2);
    Star star59 = new Star(29, 3);
    Star star60 = new Star(29, 3);
    Star star61 = new Star(0, 3);
    Star star62 = new Star(1, 3);
    Star star63 = new Star(4, 3);
    Star star64 = new Star(5, 3);
    Star star65 = new Star(7, 3);
    Star star66 = new Star(8, 3);
    Star star67 = new Star(9, 3);
    Star star68 = new Star(10, 3);
    Star star69 = new Star(11, 3);
    Star star70 = new Star(12, 3);
    Star star71 = new Star(24, 3);
    Star star72 = new Star(25, 3);
    Star star73 = new Star(28, 3);
    Star star74 = new Star(29, 3);
    Star star75 = new Star(0, 4);
    Star star76 = new Star(4, 4);
    Star star77 = new Star(11, 4);
    Star star78 = new Star(12, 4);
    Star star79 = new Star(17, 4);
    Star star80 = new Star(18, 4);
    Star star81 = new Star(19, 4);
    Star star82 = new Star(20, 4);
    Star star83 = new Star(21, 4);
    Star star84 = new Star(22, 4);
    Star star85 = new Star(28, 4);
    Star star86 = new Star(29, 4);
    Star star87 = new Star(0, 5);
    Star star88 = new Star(24, 5);
    Star star89 = new Star(25, 5);
    Star star90 = new Star(26, 5);
    Star star91 = new Star(27, 5);
    Star star92 = new Star(28, 5);
    Star star93 = new Star(29, 5);
    Star star94 = new Star(0, 6);
    Star star95 = new Star(2, 6);
    Star star96 = new Star(3, 6);
    Star star97 = new Star(4, 6);
    Star star98 = new Star(5, 6);
    Star star99 = new Star(6, 6);
    Star star100 = new Star(7, 6);
    Star star101 = new Star(8, 6);
    Star star102 = new Star(9, 6);
    Star star103 = new Star(10, 6);
    Star star104 = new Star(11, 6);
    Star star105 = new Star(12, 6);
    Star star106 = new Star(13, 6);
    Star star107 = new Star(14, 6);
    Star star108 = new Star(15, 6);
    Star star109 = new Star(16, 6);
    Star star110 = new Star(17, 6);
    Star star111 = new Star(18, 6);
    Star star112 = new Star(19, 6);
    Star star113 = new Star(20, 6);
    Star star114 = new Star(21, 6);
    Star star115 = new Star(0, 7);
    Star star116 = new Star(1, 7);
    Star star117 = new Star(2, 7);
    Star star118 = new Star(3, 7);
    Star star119 = new Star(4, 7);
    Star star120 = new Star(5, 7);
    Star star121 = new Star(6, 7);
    Star star122 = new Star(7, 7);
    Star star123 = new Star(8, 7);
    Star star124 = new Star(9, 7);
    Star star125 = new Star(10, 7);
    Star star126 = new Star(11, 7);
    Star star127 = new Star(12, 7);
    Star star128 = new Star(13, 7);
    Star star129 = new Star(14, 7);
    Star star130 = new Star(15, 7);
    Star star131 = new Star(16, 7);
    Star star132 = new Star(17, 7);
    Star star133 = new Star(18, 7);
    Star star134 = new Star(19, 7);
    Star star135 = new Star(20, 7);
    Star star136 = new Star(21, 7);
    Star star137 = new Star(22, 7);
    Star star138 = new Star(23, 7);
    Star star139 = new Star(24, 7);
    Star star140 = new Star(25, 7);
    Star star141 = new Star(26, 7);
    Star star142 = new Star(27, 7);
    Star star143 = new Star(28, 7);
    Star star144 = new Star(29, 7);

    //Set Star
    map.setStar(star1);
    map.setStar(star2);
    map.setStar(star3);
    map.setStar(star4);
    map.setStar(star5);
    map.setStar(star6);
    map.setStar(star7);
    map.setStar(star8);
    map.setStar(star9);
    map.setStar(star10);
    map.setStar(star11);
    map.setStar(star12);
    map.setStar(star13);
    map.setStar(star14);
    map.setStar(star15);
    map.setStar(star16);
    map.setStar(star17);
    map.setStar(star18);
    map.setStar(star19);
    map.setStar(star20);
    map.setStar(star21);
    map.setStar(star22);
    map.setStar(star23);
    map.setStar(star24);
    map.setStar(star25);
    map.setStar(star26);
    map.setStar(star27);
    map.setStar(star28);
    map.setStar(star29);
    map.setStar(star30);
    map.setStar(star31);
    map.setStar(star32);
    map.setStar(star33);
    map.setStar(star34);
    map.setStar(star35);
    map.setStar(star36);
    map.setStar(star37);
    map.setStar(star38);
    map.setStar(star39);
    map.setStar(star40);
    map.setStar(star41);
    map.setStar(star42);
    map.setStar(star43);
    map.setStar(star44);
    map.setStar(star45);
    map.setStar(star46);
    map.setStar(star47);
    map.setStar(star48);
    map.setStar(star49);
    map.setStar(star50);
    map.setStar(star51);
    map.setStar(star52);
    map.setStar(star53);
    map.setStar(star54);
    map.setStar(star55);
    map.setStar(star56);
    map.setStar(star57);
    map.setStar(star58);
    map.setStar(star59);
    map.setStar(star60);
    map.setStar(star61);
    map.setStar(star62);
    map.setStar(star63);
    map.setStar(star64);
    map.setStar(star65);
    map.setStar(star66);
    map.setStar(star67);
    map.setStar(star68);
    map.setStar(star69);
    map.setStar(star70);
    map.setStar(star71);
    map.setStar(star72);
    map.setStar(star73);
    map.setStar(star74);
    map.setStar(star75);
    map.setStar(star76);
    map.setStar(star77);
    map.setStar(star78);
    map.setStar(star79);
    map.setStar(star80);
    map.setStar(star81);
    map.setStar(star82);
    map.setStar(star83);
    map.setStar(star84);
    map.setStar(star85);
    map.setStar(star86);
    map.setStar(star87);
    map.setStar(star88);
    map.setStar(star89);
    map.setStar(star90);
    map.setStar(star91);
    map.setStar(star92);
    map.setStar(star93);
    map.setStar(star94);
    map.setStar(star95);
    map.setStar(star96);
    map.setStar(star97);
    map.setStar(star98);
    map.setStar(star99);
    map.setStar(star100);
    map.setStar(star101);
    map.setStar(star102);
    map.setStar(star103);
    map.setStar(star104);
    map.setStar(star105);
    map.setStar(star106);
    map.setStar(star107);
    map.setStar(star108);
    map.setStar(star109);
    map.setStar(star110);
    map.setStar(star111);
    map.setStar(star112);
    map.setStar(star113);
    map.setStar(star114);
    map.setStar(star115);
    map.setStar(star116);
    map.setStar(star117);
    map.setStar(star118);
    map.setStar(star119);
    map.setStar(star120);
    map.setStar(star121);
    map.setStar(star122);
    map.setStar(star123);
    map.setStar(star124);
    map.setStar(star125);
    map.setStar(star126);
    map.setStar(star127);
    map.setStar(star128);
    map.setStar(star129);
    map.setStar(star130);
    map.setStar(star131);
    map.setStar(star132);
    map.setStar(star133);
    map.setStar(star134);
    map.setStar(star135);
    map.setStar(star136);
    map.setStar(star137);
    map.setStar(star138);
    map.setStar(star139);
    map.setStar(star140);
    map.setStar(star141);
    map.setStar(star142);
    map.setStar(star143);
    map.setStar(star144);

    //Begin
    print("\nHey guys!");
    print("What's your name : ");
    wizard.nameWizard(stdin.readLineSync()!);
    while (win != true) {
      map.showMap();
      var choice = showNormalChoice();
      //Walk
      if (choice == "1") {
        direction = "";
        // W,a| N,w| E,d| S, s | q = quit
        while (direction != "q") {
          inputDirection();
          wizard.walk(direction, map);
          //checkAll
          wizard.checkStore();
          wizard.checkTreasure();
          loseMon = wizard.checkMonster(8);
          if (loseMon == true) {
            break;
          }
          loseBoss = wizard.checkBoss(8);
          if (loseBoss == true) {
            break;
          }

          status = wizard.checkFinish(win);
          if (status == true) {
            break;
          }
          map.showMap();
        }
      }
      //Check Bag
      else if (choice == "2") {
        wizard.checkBag();
      }
      //Exit
      else if (choice == "3") {
        checkEnd();
        break;
      }
      if (loseMon == true) {
        //do you want to play again?
        break;
      }

      if (status == true) {
        break;
      }
    }
  }

  String showNormalChoice() {
    print("Choice : ");
    print("1. Walk");
    print("2. Bag");
    print("3. Exit Game");
    var input = stdin.readLineSync()!;
    if (input == "1") {
      return "1";
    } else if (input == "2") {
      return "2";
    } else if (input == "3") {
      return "3";
    } else {
      print("\nincorrect, Please input your choice again.");
      return showNormalChoice();
    }
  }

  void showCombatChoice() {
    print("Combat choice :");
    print("1. Skills");
    print("2. Potions");
  }

  void checkEnd() {
    print("Already Exit the Game");
  }
}
