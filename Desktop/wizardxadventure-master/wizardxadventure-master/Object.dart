class Object {
  late int x;
  late int y;
  late String symbol = "";
  late int cash = 200;
  late int monHP = 100;
  late int bossHP = 1500;

  Object(String symbol, int x, int y) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
  }

  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

  String getSymbol() {
    return symbol;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  String toString() {
    return "Objects";
  }

  int fillCash() {
    int ch = cash;
    symbol = 'h';
    cash = 0;
    return ch;
  }

  int setMonl() {
    int hp = monHP;
    symbol = 'l';
    monHP = 0;
    return hp;
  }

  int getHPMon() {
    return monHP;
  }

  int setBoss_d() {
    int bhp = bossHP;
    symbol = 'd';
    bossHP = 0;
    return bhp;
  }
}
