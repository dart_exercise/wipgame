import 'dart:io';

import 'Object.dart';
import 'package:chalkdart/chalk.dart';

class Store extends Object {
  late int x, y;
  Store(int x, int y) : super("S", x, y) {
    this.x = x;
    this.y = y;
  }

  String showStore() {
    print(chalk.yellow.bold("\nStore :"));
    print(chalk.hex('#0FFBF6').bold("1. Upgrade 100 HP (50c)"));
    print(
        chalk.hex('#C3FF3C').bold("2. Upgrade Fire ball increase 3dmg. (20c)"));
    print(chalk
        .hex('#C3FF3C')
        .bold("3. Upgrade Thunder bolt increase 10dmg. (25c)"));
    print(chalk.hex('#FF61F5').bold("4. Buy HP Potion (100c)"));
    print(chalk.hex('#FF0E47').bold("5. Exit"));
    var input = stdin.readLineSync()!;
    return input;
  }
}
