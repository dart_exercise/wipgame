import 'dart:io';
import 'package:chalkdart/chalk.dart';

import 'Boss.dart';
import 'Finish.dart';
import 'Game.dart';
import 'Monster.dart';
import 'Star.dart';
import 'Wizard.dart';
import 'Store.dart';
import 'Treasure.dart';
import 'Object.dart';

class TableMap {
  late int width = 30;
  late int height = 8;
  late Game game = Game();
  late Wizard wizard;
  late Store store;
  late Treasure treasure;
  late Monster monster;
  late Boss boss;
  late Finish finish;
  late Star star;
  int objCount = 0;
  int countBoss = 0;
  List<Object> objects = [Object("", 0, 0)];
  List setListObj() {
    for (int i = 0; i < 300; i++) {
      objects.add(Object("", 0, 0));
    }
    return objects;
  }

  void addObj(Object obj) {
    objects[objCount] = obj;
    objCount++;
    // int xx = obj.getX();
    // int yy = obj.getY();
    // print(obj.getSymbol() + " Added X: $xx Y: $yy ");
  }

  void setWizard(Wizard wizard) {
    this.wizard = wizard;
    addObj(wizard);
  }

  void setStore(Store store) {
    this.store = store;
    addObj(store);
  }

  void setTreasure(Treasure treasure) {
    this.treasure = treasure;
    addObj(treasure);
  }

  void setMonster(Monster monster) {
    this.monster = monster;
    addObj(monster);
  }

  void setBoss(Boss boss) {
    this.boss = boss;
    addObj(boss);
  }

  void setFinish(Finish finish) {
    this.finish = finish;
    addObj(finish);
  }

  void setStar(Star star) {
    this.star = star;
    addObj(star);
  }

  void showMap() {
    print(chalk.red("""

██     ██   ██     ██ ████████ ██    ██   ████████   ██████      ██████ ██████  ███████ ██  ██ ████████  ██     ██
 ██   ██    ██     ██ ██    ██ ██    ██      ██    ██      ██    ██       ██    ██      ██  ██    ██      ██   ██
  ██ ██     ██  █  ██ ████████ ████████      ██    ██      ██    ████     ██    ██ ████ ██████    ██       ██ ██
   ███      ██ ███ ██ ██    ██    ██         ██    ██      ██    ██       ██    ██   ██ ██  ██    ██        ███
  █████      ███ ███  ██    ██    ██         ██      ██████      ██     ██████  ███████ ██  ██    ██       █████

    """));
    print("\n");
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        stdout.write(" ");
        printSymbolOn(x, y);
      }
      showNewLine();
    }
  }

  void printSymbolOn(int x, int y) {
    String symbol = ' ';
    for (int o = 0; o < objCount; o++) {
      if (objects[o].isOn(x, y)) {
        if (objects[o] is Wizard) {
          symbol = chalk.hex('#FFE400').bold(objects[o].getSymbol());
        } else if (objects[o] is Store) {
          symbol = chalk.hex('#F85EFF').bold(objects[o].getSymbol());
        } else if (objects[o] is Treasure) {
          symbol = chalk.hex('#E79800').bold(objects[o].getSymbol());
        } else if (objects[o] is Monster) {
          symbol = chalk.hex('#8C00E7').bold(objects[o].getSymbol());
        } else if (objects[o] is Boss) {
          symbol = chalk.hex('#FF0000').bold(objects[o].getSymbol());
        } else if (objects[o] is Finish) {
          symbol =
              chalk.yellow(chalk.ansi(31).bgAnsi(93)(objects[o].getSymbol()));
        } else if (objects[o] is Star) {
          symbol = chalk.onAnsi256(10)(objects[o].getSymbol());
        }
        // symbol = objects[o].getSymbol();
      }
    }
    stdout.write(symbol);
  }

  void showNewLine() {
    print("\n");
  }

  bool inMap(int x, int y) {
    // x -> 0-(width-1), y -> 0-(height-1)
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isStore(int x, int y) {
    return store.isOn(x, y);
  }

  bool isTreasure(int x, int y) {
    return treasure.isOn(x, y);
  }

  bool isMonster(int x, int y) {
    return monster.isOn(x, y);
  }

  bool isBoss(int x, int y) {
    return boss.isOn(x, y);
  }

  bool isFinish(int x, int y) {
    return finish.isOn(x, y);
  }

  bool isStar(int x, int y) {
    for (int i = 0; i < objCount; i++) {
      if (objects[i] is Star && objects[i].isOn(x, y)) {
        return true;
      }
    }
    return false;
    // return star.isOn(x, y);
  }

//Treasure Set
  int reCash(int x, int y) {
    for (int i = 0; i < objCount; i++) {
      if (objects[i] is Treasure && objects[i].isOn(x, y)) {
        Object ts = objects[i];
        return ts.fillCash();
      }
    }
    return 0;
  }

//Monster Set
  int setMon(int x, int y) {
    for (int i = 0; i < objCount; i++) {
      if (objects[i] is Monster &&
          objects[i].isOn(x, y) &&
          (objects[i].getX() == x && objects[i].getY() == y)) {
        Object mnl = objects[i];
        return mnl.setMonl();
      }
    }
    return 0;
  }

  String monsterl(int x, int y) {
    for (int i = 0; i < objCount; i++) {
      if (objects[i] is Monster &&
          objects[i].isOn(x, y) &&
          objects[i].getSymbol() == "l") {
        return "l";
      }
    }
    return "";
  }

  int checkMonsterHP(int x, int y) {
    for (int i = 0; i < objCount; i++) {
      if (objects[i] is Monster && objects[i].isOn(x, y)) {
        Object mn = objects[i];
        return mn.setMonl();
      }
    }
    return 0;
  }

  bool attackMonster(int x, int y, int count, int hpMonster) {
    int potions = wizard.getPotions();
    int wizHP = wizard.getHPWiz();
    bool loseMon = false;
    while (loseMon != true) {
      print(chalk.hex('#FF783C').bold("Monster HP : $hpMonster"));
      print(chalk.hex('#3CF2FF').bold("Your HP : $wizHP"));
      print(chalk.hex('#FBFB0F').bold("\n[Your Turn]"));
      game.showCombatChoice();
      var input = stdin.readLineSync()!;
      //Skills
      if (input == "1") {
        //show skill choice
        var skill = wizard.showSkills(count);
        //Fire ball
        if (skill == "1") {
          print(chalk
              .hex('#0FFBF6')
              .bold("...Fire ball attacked the monster..."));
          int dmg1 = wizard.getDmg1();
          print(chalk.hex('#0FFBF6').bold("\nMonster take $dmg1" " damage"));
          hpMonster -= dmg1;
          if (hpMonster <= 0) {
            hpMonster = 0;
            print(chalk.hex('#45FF00').bold("\nYou killed monster"));
            print(chalk.hex('#8CFF00').bold("receive 200cash"));
            wizard.cash += 200;
            //set l
            setMon(x, y);
            var key = stdin.readLineSync()!;
            loseMon = false;
            break;
          }
          print(chalk.hex('#FF783C').bold("\nMonster HP : $hpMonster"));
          print(chalk.hex('#3CF2FF').bold("Your HP : $wizHP"));
        }
        //Thunder bolt
        else if (skill == "2") {
          int dmg2 = wizard.getDmg2();
          hpMonster -= dmg2;
          if (hpMonster <= 0 && count > 0) {
            hpMonster = 0;
            print(chalk.hex('#45FF00').bold("\nYou killed monster"));
            print(chalk.hex('#8CFF00').bold("receive 200cash"));
            wizard.cash += 200;
            //set l
            setMon(x, y);
            var key = stdin.readLineSync()!;
            loseMon = false;
            break;
          }
          if (count > 0) {
            print(chalk
                .hex('#0FFBF6')
                .bold("...Thunder bolt attacked to monster!!..."));
            print(chalk.hex('#0FFBF6').bold("\nmonster take $dmg2" " damage"));
            print(chalk.hex('#FF783C').bold("\nMonster HP : $hpMonster"));
            print(chalk.hex('#3CF2FF').bold("Your HP : $wizHP"));
            count--;
          } else {
            print(chalk.hex('#3CF2FF').bold("Cannot use this skill"));
            attackMonster(x, y, count, hpMonster + dmg2);
          }
        } else if (skill == "3") {
          attackMonster(x, y, count, hpMonster);
        }
      }
      //Potions
      else if (input == "2") {
        //show potions already have and choice use or not or back
        var usePo = wizard.showPotions();
        if (usePo == "1") {
          if (wizard.getPotions() > 0) {
            print(chalk
                .hex('#3CF2FF')
                .bold("You drink the bottle of potions and restore 200HP"));
            //Set DefaultHP
            if (wizHP + 200 > wizard.getHPDefault()) {
              wizard.setHPWiz(wizard.getHPDefault());
            } else {
              wizHP += 200;
            }
            potions--;
            wizard.setHPWiz(wizHP);
            wizard.setPotions(potions);
          } else {
            print(chalk
                .hex('#3CF2FF')
                .bold("Potions are out of stock and cannot be used"));
            attackMonster(x, y, count, hpMonster);
          }
        } else if (usePo == "2") {
          attackMonster(x, y, count, hpMonster);
        } else if (usePo != "1" || usePo != "2") {
          attackMonster(x, y, count, hpMonster);
        }
      }
      // != 1, 2
      else if (input != "1" && input != "2") {
        print(chalk.hex('#3CF2FF').bold("Input choice again"));
        attackMonster(x, y, count, hpMonster);
      }

      //monter attack wizard
      var key = stdin.readLineSync()!;
      print(chalk.hex('#FF783C').bold("[Monster Turn]"));
      print(chalk.hex('#FF783C').bold("\nSkill Claw !!"));
      print(chalk.hex('#0FFBF6').bold("You take 10 damage"));
      key = stdin.readLineSync()!;
      if (wizHP - 10 > 0) {
        wizHP = wizHP - 10;
      } else {
        wizHP = 0;
      }
      wizard.setHPWiz(wizHP);
      //if wizard hp <= 0 lose
      if (wizard.getHPWiz() <= 0) {
        print(chalk.hex('#183EFA')(""" 

██    ██  ██████  ██    ██ 
 ██  ██  ██    ██ ██    ██ 
  ████   ██    ██ ██    ██ 
   ██    ██    ██ ██    ██ 
   ██     ██████   ██████ 


██      ██████  ██████ ██████ 
██     ██    ██ ██     ██ 
██     ██    ██ ██████ ██████  
██     ██    ██     ██ ██     
██████  ██████  ██████ ██████ 

        """));
        loseMon = true;
        return loseMon;
      }
    }
    if (loseMon != true) {
      return false;
    } else {
      return true;
    }
  }

  //Boss Set
  int setBoss_d(int x, int y) {
    for (int i = 0; i < objCount; i++) {
      if (objects[i] is Boss &&
          objects[i].isOn(x, y) &&
          (objects[i].getX() == x && objects[i].getY() == y)) {
        Object bsd = objects[i];
        return bsd.setBoss_d();
      }
    }
    return 0;
  }

  String boss_d(int x, int y) {
    for (int i = 0; i < objCount; i++) {
      if (objects[i] is Boss &&
          objects[i].isOn(x, y) &&
          objects[i].getSymbol() == "d") {
        return "d";
      }
    }
    return "";
  }

  bool attackBoss(int x, int y, int count, int hpBoss) {
    String nameWiz = wizard.getName();
    int potions = wizard.getPotions();
    int wizHP = wizard.getHPWiz();
    bool loseBoss = false;
    while (loseBoss != true) {
      print(chalk.hex('#FF783C').bold("Boss HP : $hpBoss"));
      print(chalk.hex('#3CF2FF').bold("Your HP : $wizHP"));
      print(chalk.hex('#FBFB0F').bold("\n[Your Turn]"));
      game.showCombatChoice();
      var input = stdin.readLineSync()!;
      //Skills
      if (input == "1") {
        //show skill choice
        var skill = wizard.showSkills(count);
        //Fire ball
        if (skill == "1") {
          print(chalk.hex('#0FFBF6').bold(
              "...$nameWiz cast a spell \"Fire Ball\" and attacked the Boss..."));
          int dmg1 = wizard.getDmg1();
          print(chalk.hex('#0FFBF6').bold("\nBoss take $dmg1" " damage"));
          hpBoss -= dmg1;
          if (hpBoss <= 0) {
            hpBoss = 0;
            print(chalk.hex('#0FFBF6').bold("\nYou can take down BOSS!"));
            print(chalk.hex('#0FFBF6').bold("The way is clear"));
            //set d
            setBoss_d(x, y);
            var key = stdin.readLineSync()!;
            loseBoss = false;
            break;
          }
          print(chalk.hex('#FF783C').bold("\nBoss HP : $hpBoss"));
          print(chalk.hex('#3CF2FF').bold("Your HP : $wizHP"));
        }
        //Thunder bolt
        else if (skill == "2") {
          int dmg2 = wizard.getDmg2();
          hpBoss -= dmg2;
          if (hpBoss <= 0 && count >= 0) {
            hpBoss = 0;
            print(chalk.hex('#0FFBF6').bold("\nYou can take down BOSS!"));
            print(chalk.hex('#0FFBF6').bold("The way is clear"));
            //set d
            setBoss_d(x, y);
            var key = stdin.readLineSync()!;
            loseBoss = false;
            break;
          }
          if (count > 0) {
            print(chalk.hex('#0FFBF6').bold("Thunder bolt!!"));
            print(chalk
                .hex('#0FFBF6')
                .bold("...Lightning strikes to The boss..."));
            print(chalk.hex('#0FFBF6').bold("\nBoss take $dmg2" " damage"));
            print(chalk.hex('#FF783C').bold("\nBoss HP : $hpBoss"));
            print(chalk.hex('#3CF2FF').bold("Your HP : $wizHP"));
            count--;
          } else {
            print(chalk.hex('#3CF2FF').bold("Cannot use this skill"));
            attackBoss(x, y, count, hpBoss + dmg2);
          }
        } else if (skill == "3") {
          attackBoss(x, y, count, hpBoss);
        }
      }
      //Potions
      else if (input == "2") {
        //show potions already have and choice use or not or back
        var usePo = wizard.showPotions();
        if (usePo == "1") {
          if (wizard.getPotions() > 0) {
            print(chalk
                .hex('#3CF2FF')
                .bold("You drink the bottle of potions and restore 200HP"));
            //Set DefaultHP
            if (wizHP + 200 > wizard.getHPDefault()) {
              wizard.setHPWiz(wizard.getHPDefault());
            } else {
              wizHP += 200;
            }
            potions--;
            wizard.setHPWiz(wizHP);
            wizard.setPotions(potions);
          } else {
            print(chalk
                .hex('#3CF2FF')
                .bold("Potions are out of stock and cannot be used"));
            attackBoss(x, y, count, hpBoss);
          }
        } else if (usePo == "2") {
          attackBoss(x, y, count, hpBoss);
        }
      }
      // != 1, 2
      else if (input != "1" && input != "2") {
        print(chalk.hex('#3CF2FF').bold("Input choice again"));
        attackBoss(x, y, count, hpBoss);
      }

      //Boss attack wizard
      String skillBoss;
      //if Boss HP low to 600 atk +90 = 180
      if (hpBoss < 600 && hpBoss != 0 && countBoss == 0) {
        int atk = boss.getAtk() + 90;
        boss.setAtk(atk);
        skillBoss = "Bleakwind Storm!!";
        int atkBDmg = boss.getAtk();
        print(chalk.hex('#3CF2FF').bold("\n...The Boss HP low to 600..."));
        print(chalk
            .hex('#3CF2FF')
            .bold("\nBoss get buff atk increase to $atkBDmg damage"));
        var key = stdin.readLineSync()!;
        print(chalk.hex('#FF783C').bold("[Boss Turn]"));
        print(chalk.hex('#FF4A3C').bold("\n$skillBoss"));
        int bossDmg = boss.getAtk();
        print(chalk.hex('#45FF00').bold("You take $bossDmg damage"));
        key = stdin.readLineSync()!;
        if (wizHP - bossDmg > 0) {
          wizHP = wizHP - bossDmg;
        } else {
          wizHP = 0;
        }
        wizard.setHPWiz(wizHP);
        countBoss -= 99999999;
      } else if (hpBoss < 600 && hpBoss != 0) {
        skillBoss = "Bleakwind Storm!!";
        var key = stdin.readLineSync()!;
        print(chalk.hex('#FF783C').bold("[Boss Turn]"));
        print(chalk.hex('#FF4A3C').bold("\n$skillBoss"));
        int bossDmg = boss.getAtk();
        print(chalk.hex('#45FF00').bold("You take $bossDmg damage"));
        key = stdin.readLineSync()!;
        if (wizHP - bossDmg > 0) {
          wizHP = wizHP - bossDmg;
        } else {
          wizHP = 0;
        }
        wizard.setHPWiz(wizHP);
      } else {
        skillBoss = "Air Cutter!!";
        var key = stdin.readLineSync()!;
        print(chalk.hex('#FF783C').bold("[Boss Turn]"));
        print(chalk.hex('#3CFF88').bold("\n$skillBoss"));
        int bossDmg = boss.getAtk();
        print(chalk.hex('#45FF00').bold("You take $bossDmg damage"));
        key = stdin.readLineSync()!;
        if (wizHP - bossDmg > 0) {
          wizHP = wizHP - bossDmg;
        } else {
          wizHP = 0;
        }
        wizard.setHPWiz(wizHP);
      }

      //if wizard hp <= 0 lose
      if (wizard.getHPWiz() <= 0) {
        print(chalk.hex('#183EFA')(""" 

██    ██  ██████  ██    ██ 
 ██  ██  ██    ██ ██    ██ 
  ████   ██    ██ ██    ██ 
   ██    ██    ██ ██    ██ 
   ██     ██████   ██████ 


██      ██████  ██████ ██████ 
██     ██    ██ ██     ██ 
██     ██    ██ ██████ ██████  
██     ██    ██     ██ ██     
██████  ██████  ██████ ██████ 

        """));
        loseBoss = true;
        return loseBoss;
      }
    }
    if (loseBoss != true) {
      return false;
    } else {
      return true;
    }
  }
}
